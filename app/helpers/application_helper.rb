module ApplicationHelper
  def paypal_authorize_url(token)
    if Rails.env.production?
      "https://www.paypal.com/webscr&cmd=_grant-permission&request_token=#{token}"
    else
      "https://www.sandbox.paypal.com/webscr&cmd=_grant-permission&request_token=#{token}"
    end
  end
end
