class WelcomeController < ApplicationController
  def index
    @token = Cabernet::PaypalPermissionGateway.request_permissions(welcome_show_url)
  end
  
  def show
    @token_response = Cabernet::PaypalPermissionGateway.get_access_token(params["request_token"], params["verification_code"])
  end
end
