
## Versions

Ruby  : 2.2.3
Rails : 4.2.4

paypal-sdk-permissions : 1.96.4

## Usage

To authorize your shopping cart, in your controller call:

```ruby
@token = Cabernet::PaypalPermissionGateway.request_permissions(access_token_url)
```

The access_token_url is the call back url that will execute next. This call will return a token. This token is used in the 'Grant Permission' call in the view:

```ruby
<%= link_to 'Grant Permission to Zepho', paypal_authorize_url(@token) %>
```

The paypal_authorize_url is a helper:

```ruby
  def paypal_authorize_url(token)
    if Rails.env.production?
      "https://www.paypal.com/webscr&cmd=_grant-permission&request_token=#{token}"
    else
      "https://www.sandbox.paypal.com/webscr&cmd=_grant-permission&request_token=#{token}"
    end
  end
```

It generates the URL to redirect the user to login to paypal and grant permission to the shopping cart. This call returns verification code and token to the call back URL. The controller makes the call:
  
```ruby
Cabernet::PaypalPermissionGateway.get_access_token(request_token, verification_code)
```

to get the token object that contains token and tokenSecret that is saved in the database. If these fields are populated, it means a merchant has granted permission to the shopping cart.

## Flow

Step 1

Request:

```ruby
require 'paypal-sdk-permissions'
@api = PayPal::SDK::Permissions::API.new
```

# Build request object

```ruby
@request_permissions = @api.build_request_permissions({
  :scope => ["EXPRESS_CHECKOUT"],
  :callback => "https://paypal-sdk-samples.herokuapp.com/permissions/get_access_token" })
```

# Make API call & get response

```ruby
@request_permissions_response = @api.request_permissions(@request_permissions)
```

# Access Response

```ruby
if @request_permissions_response.success?
  @request_permissions_response.token
else
  @request_permissions_response.error
end
```

Response:

```ruby
{
  :responseEnvelope => {
    :timestamp => "2015-08-23T22:58:59-07:00",
    :ack => "Success",
    :correlationId => "f21548468e6ed",
    :build => "2210301" },
  :token => "AAAAAAAeo5VWpHEN42JS" }
```ruby

Step 2 :

Click Link to be redirected to paypal, login and grant permission. This call returns verification token and token to the call back URL.

Step 3 :

Get Access Token call:

```ruby
require 'paypal-sdk-permissions'
@api = PayPal::SDK::Permissions::API.new
```

# Build request object

```ruby
@get_access_token = @api.build_get_access_token({
  :token => "AAAAAAAeo5VWpHEN42JS",
  :verifier => "e-wA.lU4OT0VS59H2vnlHA" })
```

# Make API call & get response

```ruby
@get_access_token_response = @api.get_access_token(@get_access_token)
```

# Access Response

```ruby
if @get_access_token_response.success?
  @get_access_token_response.scope
  @get_access_token_response.token
  @get_access_token_response.tokenSecret
else
  @get_access_token_response.error
end
```

Response:

```ruby
{
  :responseEnvelope => {
    :timestamp => "2015-08-23T23:03:39-07:00",
    :ack => "Success",
    :correlationId => "073f80a8f3ab4",
    :build => "2210301" },
  :scope => ["EXPRESS_CHECKOUT"],
  :token => "lDXqPnqlG5pV2rO7xkH29I5oMxTszUB48pV7ihFh4FrOweqLI4VkAw",
  :tokenSecret => "7UtTKDCm4voy.XDTKyQP1-XibD0" }
```
  
Save the token and tokenSecret in the database so that we know who has already authorized the shopping cart.